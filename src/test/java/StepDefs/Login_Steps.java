package StepDefs;

import Pages.LoginPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login_Steps {
	
	// login page object creation	
	LoginPage loginPage = new LoginPage();
	
	@Given("^I navigate to voyanta login page$")
	public void i_navigate_to_voyant_login_page() throws Throwable {
	   loginPage.gotoHomePage();
	   loginPage.navigateToLoginPage();
	}

	@Given("^Enter email address \"([^\"]*)\"$")
	public void enter_email_address(String emailAddress) throws Throwable {
		
		loginPage.enterEmailAddress(emailAddress);
	}

	@Given("^Enter password \"([^\"]*)\"$")
	public void enter_password(String password) throws Throwable {		
	
		loginPage.enterPassword(password);
	
	}
	
	@When("^I click on Login button$")
	public void i_click_on_Login_button() throws Throwable {
	   
		loginPage.clickLoginButton();
	}

	@Then("^Overviews page opened successfully$")
	public void overviews_page_opened_successfully() throws Throwable {
	   
		loginPage.IsOverviewPageOpened();
	}

}
