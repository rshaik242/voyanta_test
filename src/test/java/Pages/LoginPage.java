package Pages;

import java.util.concurrent.TimeUnit;

import javax.swing.plaf.synth.Region;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
private WebDriver driver;
String url = "http://www.voyanta.com/";
	
 public LoginPage(){
	 // Initialise firefox browser
	 driver = new FirefoxDriver();
	 driver.manage().window().maximize();
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 
	 PageFactory.initElements(driver, this);	 
 }
 
// Element locators
 @FindBy(how = How.LINK_TEXT, using = "Login") 
 private WebElement login;
 
 @FindBy(how = How.ID, using = "email") 
 private WebElement emailAddress;
 
 @FindBy(how = How.ID, using = "password") 
 private WebElement password;
 
 @FindBy(how = How.NAME, using = "save") 
 private WebElement loginButton;
 
 
 public void gotoHomePage(){
	 // open home page url
	 driver.get(url);
	 
	 // assert homepage title
	 Assert.assertEquals("Real Estate Investment Management Software | Voyanta", driver.getTitle());
 }
 
 public void navigateToLoginPage() throws InterruptedException {
	 // click on login link on home page
	 login.click();
	 
	 // wait for one second to pageload
	 Thread.sleep(1000);
	 
	 //assert login page title to make sure login page opened
	 Assert.assertEquals("Voyanta - Login", driver.getTitle()); 	 
 }
 
 public void Login(String email, String password) throws Exception{
	 
	 enterEmailAddress(email);	 
	 enterPassword(password);		
	 clickLoginButton();
	 
 }

 public void enterEmailAddress(String email) {
	    emailAddress.clear();
	    
	    // enter email address into email address field
		emailAddress.sendKeys(email);
	}
 
 public void enterPassword(String pwd) {
		password.clear();
		
		// enter password into password field
		password.sendKeys(pwd);
	}
 
public void clickLoginButton() throws InterruptedException {
	   // click on login button on login page
	   loginButton.click();	 
	   Thread.sleep(2000);
}

public void IsOverviewPageOpened() {
	
	// as per the exercise, I am adding assert to check Overview page title
	Assert.assertEquals("Overview page not opened", "Overview", driver.getTitle());
}

}
