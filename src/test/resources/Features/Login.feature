
Feature: Voyanta login Test 

Scenario Outline: Login scenario
	Given I navigate to voyanta login page
	And Enter email address "<email address>"
	And Enter password "<password>"
	When I click on Login button
	Then Overviews page opened successfully
Examples:
	| email address| password |
	|test@test.com | test     |
	
test#1